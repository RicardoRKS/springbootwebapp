package axide.springboot.SimpleWebApp.model;


import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractModel implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    @Version
    protected int version;

    @Override
    public Integer getId() {
        return id;
    }


    @Override
    public void setId(Integer id) {
        this.id = id;
    }
}
