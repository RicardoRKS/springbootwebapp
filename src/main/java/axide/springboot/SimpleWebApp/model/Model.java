package axide.springboot.SimpleWebApp.model;

public interface Model {

    Integer getId();

    void setId(Integer id);
}
