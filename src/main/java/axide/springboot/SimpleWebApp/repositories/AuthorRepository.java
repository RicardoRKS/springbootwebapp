package axide.springboot.SimpleWebApp.repositories;

import axide.springboot.SimpleWebApp.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> {
}
