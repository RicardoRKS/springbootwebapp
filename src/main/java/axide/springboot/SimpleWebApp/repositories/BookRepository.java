package axide.springboot.SimpleWebApp.repositories;

import axide.springboot.SimpleWebApp.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {
}
