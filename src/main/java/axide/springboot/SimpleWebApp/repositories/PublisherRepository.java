package axide.springboot.SimpleWebApp.repositories;

import axide.springboot.SimpleWebApp.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher,Integer> {
}
